//
//  TutorViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 24/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit

protocol EngagedViewDelegate {
    func schedule(sender: EngagedView)
    func seePlanning(sender: EngagedView)
    func viewProfileEngaged(sender: EngagedView)
}

class EngagedView: UIView {
    @IBOutlet weak var imageTutor: UIImageView!
    @IBOutlet weak var fullNameTutor: UILabel!
    
    var tutor: Nutritionist! {
        didSet {
            //            tutor.fetchInBackgroundWithBlock { (_, error) in
            //                self.tutor.profile.fetchIfNeededInBackgroundWithBlock { (obj, error) in
            //                    Server.checkIsOnline(self.tutor.profile.user) { (isOnline) in
            //                        if let isOnline = isOnline as? Bool {
            //                            if !isOnline {
            //                                dispatch_async(dispatch_get_main_queue(), {
            //                                    self.imageTutor.layer.borderWidth = 0
            //                                })
            //                            }
            //                        }
            //                    }
            //                    dispatch_async(dispatch_get_main_queue(), {
            //                        self.fullNameTutor.text = self.tutor.profile.fullName
            //                    })
            //                    self.tutor.profile.image?.getDataInBackgroundWithBlock { (data, error) in
            //                        if error == nil {
            //                            dispatch_async(dispatch_get_main_queue(), {
            //                                self.imageTutor.image = UIImage(data: data!)
            //                            })
            //                        }
            //                    }
            //                }
            //            }
        }
    }
    var delegate: EngagedViewDelegate?
    
    @IBAction func schedule(sender: UIButton) {
        delegate?.schedule(self)
    }
    
    @IBAction func seePlanning(sender: UIButton) {
        delegate?.seePlanning(self)
    }
    
    @IBAction func viewProfile(sender: UIButton) {
        delegate?.viewProfileEngaged(self)
    }
}

protocol TutorTableViewCellDelegate {
    func viewProfile(sender: TutorTableViewCell)
    func engage(sender: TutorTableViewCell)
}

class TutorTableViewCell: UITableViewCell {
    @IBOutlet weak var imageTutor: UIImageView!
    @IBOutlet weak var fullNameTutor: UILabel!
    
    var tutor: Nutritionist! {
        didSet {
            tutor.profile.fetchIfNeededInBackgroundWithBlock { (obj, error) in
                //                Server.checkIsOnline(self.tutor.profile.user) { (isOnline) in
                //                    if let isOnline = isOnline as? Bool {
                //                        if isOnline {
                //                            dispatch_async(dispatch_get_main_queue(), {
                //                                self.imageTutor.layer.borderUIColor = UIColor(netHex: 0x228A97)
                //                                self.imageTutor.layer.borderWidth = 3
                //                            })
                //                        }
                //                    }
                //                }
                dispatch_async(dispatch_get_main_queue(), {
                    self.fullNameTutor.text = self.tutor.profile.fullName
                })
                self.tutor.profile.image?.getDataInBackgroundWithBlock { (data, error) in
                    if error == nil {
                        dispatch_async(dispatch_get_main_queue(), {
                            self.imageTutor.image = UIImage(data: data!)
                        })
                    }
                }
            }
        }
    }
    
    var delegate: TutorTableViewCellDelegate?
    
    @IBAction func viewProfile(sender: UIButton) {
        delegate?.viewProfile(self)
    }
    
    @IBAction func engage(sender: UIButton) {
        delegate?.engage(self)
    }
}

class TutorViewController: ViewController {
    
    @IBOutlet weak var stackEngageds: UIStackView!
    @IBOutlet weak var stackEngagedsHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    var tutors: [Nutritionist]! = []
    
    var noneEngagedView: UIView!
    
    var container: ContainerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateTutors()
        //        Server.tutors { (obj) in
        //            self.tutors = obj as! [Nutritionist]
        //            self.tableView.reloadData()
        //        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func explore(sender: UIBarButtonItem) {
        let nextVC = storyboard!.instantiateViewControllerWithIdentifier(SearchViewController.IDENTIFIER) as!
        SearchViewController
        nextVC.delegate = self
        nextVC.type = .Tutors
        nextVC.skip = 0
        
        self.presentViewController(nextVC, animated: true, completion: nil)
    }
    
    func updateTutors() {
        for v in stackEngageds.arrangedSubviews {
            stackEngageds.removeArrangedSubview(v)
        }
        
        if Server.customer!.engaged.count == 0 {
            let noneEngagedView = UINib(nibName: "EngagedView", bundle: nil)
                .instantiateWithOwner(self, options: nil)[1] as! UIView
            stackEngageds.addArrangedSubview(noneEngagedView)
            return
        }
        
        stackEngagedsHeight.constant = stackEngagedsHeight.constant * CGFloat(Server.customer!.engaged.count)
        
        for tutor in Server.customer!.engaged {
            let engagedView = UINib(nibName: "EngagedView", bundle: nil)
                .instantiateWithOwner(self, options: nil)[0] as! EngagedView
            engagedView.tutor = tutor
            engagedView.delegate = self
            stackEngageds.addArrangedSubview(engagedView)
        }
        
        stackEngageds.updateConstraints()
    }
}

extension TutorViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tutors.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("tutor-cell") as! TutorTableViewCell
        
        cell.tutor = tutors[indexPath.row]
        cell.delegate = self
        
        return cell
    }
}

extension TutorViewController: TutorTableViewCellDelegate, EngagedViewDelegate {
    func viewProfile(sender: TutorTableViewCell) {
        ((container.viewControllers[4] as! UINavigationController).topViewController as! ProfileViewController)
            .viewType = .Nutritionist
        container.moveToViewControllerAtIndex(4)
    }
    
    func engage(sender: TutorTableViewCell) {
        if Server.customer!.engaged.count > 0 {
            for e in Server.customer!.engaged {
                if sender.tutor.objectId == e.objectId {
                    return
                }
            }
        }
        
        let message = Server.customer!.engaged.count == 0 ? "Você deseja engajar este tutor?" : "Você deseja trocar de tutor?"
        
        let alert = UIAlertController(title: "Alerta"
            , message: message
            , preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Sim"
            , style: .Default
            , handler: { (_) in
                if Server.customer?.engaged.count == 1 {
                    Server.customer!.engaged.removeAtIndex(0)
                }
                Server.customer?.engaged.append(sender.tutor)
                Server.customer?.saveInBackgroundWithBlock({ (success, error) in
                    dispatch_async(dispatch_get_main_queue(), {
                        let alert = UIAlertController(title: "Mensagem"
                            , message: "Você engajou este tutor!"
                            , preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Ok"
                            , style: .Cancel
                            , handler: nil))
                        self.presentViewController(alert
                            , animated: true
                            , completion: nil)
                        self.updateTutors()
                    })
                })
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar"
            , style: .Cancel
            , handler: nil))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func seePlanning(sender: EngagedView) {
        
    }
    
    func viewProfileEngaged(sender: EngagedView) {
        ((container.viewControllers[4] as! UINavigationController).topViewController as! ProfileViewController)
            .viewType = .Nutritionist
        container.moveToViewControllerAtIndex(4)
    }
    
    func schedule(sender: EngagedView) {
        
    }
}

extension TutorViewController: SearchViewControllerDelegate {
    func searchViewController(results: [AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.tutors = results as! [Nutritionist]
        self.tableView.reloadData()
    }
}