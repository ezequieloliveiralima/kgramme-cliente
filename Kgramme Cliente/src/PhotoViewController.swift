//
//  PhotoViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 23/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import MBProgressHUD

class ToolBarImagePicker: UIView {
    @IBOutlet weak var addDescription: UIButton!
    @IBOutlet weak var takeImage: UIButton!
    @IBOutlet weak var showLevelColors: UIButton!
}

class ColorView: UIView {
    @IBOutlet weak var titleColor: UILabel!
    @IBOutlet weak var descriptionColor: UITextView!
}

class PhotoViewController: UIImagePickerController, IndicatorInfoProvider {
    static let IDENTIFIER = "Photo"
    
    var customToolBar: ToolBarImagePicker!
    var toolBar: ToolBarImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sourceType = .Camera
        self.allowsEditing = true
        self.showsCameraControls = false
        self.delegate = self
        
        toolBar = UINib(nibName: "ToolBarImagePicker", bundle: nil)
            .instantiateWithOwner(self, options: nil)[0] as! ToolBarImagePicker
        toolBar.frame = CGRect(x: 0
            , y: self.getScreenBounds().height - 132.0
            , width: self.getScreenBounds().width
            , height: 44.0)
        
        self.view.addSubview(toolBar)
        self.edgesForExtendedLayout = .None
        
        //let f = self.cameraOverlayView!.frame
//        cameraOverlayView?.addSubview(toolBar)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        toolBar.takeImage.addTarget(self
            , action: #selector(PhotoViewController.takeImage(_:))
            , forControlEvents: .PrimaryActionTriggered)
        toolBar.takeImage.setImage(Toucan(image: UIImage(named: "target")!)
            .resizeByCropping(CGSize(width: 30.0, height: 30.0)).image, forState: .Normal)
        toolBar.takeImage.tintColor = UIColor(netHex: 0x333333)
        toolBar.takeImage.setTitle(nil, forState: .Normal)
        
        toolBar.addDescription.enabled = false
        toolBar.showLevelColors.enabled = false
    }
    
    func takeImage(sender: UIButton) {
        toolBar.addDescription.enabled = true
        toolBar.showLevelColors.enabled = true
        self.takePicture()
        self.setEditing(true, animated: true)
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Foto"
    }
}

extension PhotoViewController: UINavigationControllerDelegate
, UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let preview = storyboard!.instantiateViewControllerWithIdentifier(PhotoViewControllerPreview.IDENTIFIER) as! PhotoViewControllerPreview
            preview.image = image
            self.pushViewController(preview, animated: false)
        }
    }
}

class PhotoViewControllerPreview: ViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    static let IDENTIFIER = "ImagePreview"
    
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = image
        
        let nav = self.navigationController as? PhotoViewController
        let btnTakeImage = nav!.toolBar.takeImage
        btnTakeImage.setImage(UIImage(named: "close")!, forState: .Normal)
        btnTakeImage.tintColor = UIColor.redColor()
        btnTakeImage.addTarget(self
            , action: #selector(PhotoViewControllerPreview.cancel(_:))
            , forControlEvents: .PrimaryActionTriggered)
        
        nav?.toolBar.addDescription.enabled = true
        nav?.toolBar.showLevelColors.enabled = true
        
        nav?.toolBar.showLevelColors.addTarget(self
            , action: #selector(PhotoViewControllerPreview.showLevelColors(_:))
            , forControlEvents: .PrimaryActionTriggered)
        nav?.toolBar.addDescription.addTarget(self
            , action: #selector(PhotoViewControllerPreview.sendDish(_:))
            , forControlEvents: .PrimaryActionTriggered)
    }
    
    func cancel(sender: UIButton) {
        sender.setImage(Toucan(image: UIImage(named: "target")!)
            .resizeByCropping(CGSize(width: 30.0, height: 30.0)).image
            , forState: .Normal)
        
        let nav = self.navigationController as? PhotoViewController
        nav?.toolBar.addDescription.enabled = false
        nav?.toolBar.showLevelColors.enabled = false
        nav?.toolBar.takeImage.tintColor = UIColor(netHex: 0x333333)
        
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    func showLevelColors(sender: UIButton) {
        let colors = image.dominantColors()
        let view = storyboard?.instantiateViewControllerWithIdentifier(ColorsViewController.IDENTIFIER) as! ColorsViewController
        view.colors = colors
        self.presentViewController(view, animated: true, completion: nil)
    }
    
    func sendDish(sender: UIButton) {
        let sendDish = storyboard!.instantiateViewControllerWithIdentifier(SendDish.IDENTIFIER) as! SendDish
        sendDish.image = image
        
        self.presentViewController(sendDish
            , animated: true
            , completion: nil)
    }
}

class SendDish: ViewController {
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnTutor: UIButton!
    
    static let IDENTIFIER = "Send dish"
    
    let placeholder = "Descreva o que está comendo..."
    
    var image: UIImage!
    var tutors: [Nutritionist]!
    var selectedTutor: Nutritionist?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tutors = Server.customer?.engaged
        textView.text = placeholder
        textView.textColor = UIColor(netHex: 0xCCCCCC)
    }
    
    @IBAction func close(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func selectTutor(sender: UIButton) {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        let actionSheet = UIAlertController(title: "Tutor"
            , message: "Selecione o seu tutor"
            , preferredStyle: .ActionSheet)
        
        for tutor in tutors {
            tutor.fetchInBackgroundWithBlock({ (obj, error) in
                actionSheet.addAction(UIAlertAction(title: tutor.profile.fullName, style: .Default, handler: { (_) -> Void in
                    self.selectedTutor = tutor
                    dispatch_async(dispatch_get_main_queue(), {
                        self.btnTutor.setTitle(tutor.profile.fullName, forState: .Normal)
                        self.btnTutor.titleLabel?.adjustsFontSizeToFitWidth = true
                    })
                }))
                if tutor == self.tutors.last {
                    MBProgressHUD.hideHUDForView(self.view, animated: true)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.presentViewController(actionSheet, animated: true, completion: nil)
                    })
                }
            })
        }
    }
    
    @IBAction func sendDish(sender: UIButton) {
        var hasError: Bool = false
        var errorMessage: String = ""
        
        if textView.text == placeholder {
            errorMessage = "Adicione uma descição ao prato."
            hasError = true
        }
        
        if selectedTutor == nil {
            errorMessage = "Selecione um tutor"
            hasError = true
        }
        
        if hasError {
            let alert = UIAlertController(title: "Erro"
                , message: errorMessage
                , preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
            return
        }
        
        let dish = Dish()
        
        dish.image          = image.toPFFile()
        // dish.costumer       = Server.customer!
        dish.tutor          = selectedTutor!
        dish.aditionalInfo  = textView.text
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        dish.saveInBackgroundWithBlock { (success, error) in
            dispatch_async(dispatch_get_main_queue(), {
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                self.dismissViewControllerAnimated(true, completion: nil)
            })
            if error != nil {
                NSNotificationCenter.defaultCenter().postNotificationName("error"
                    , object: nil
                    , userInfo: ["error": error!])
            }
        }
    }
}

extension SendDish: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        textView.textColor = UIColor.blackColor()
        if textView.text == placeholder {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = UIColor(netHex: 0xCCCCCC)
        }
    }
}

class NoCameraViewController: ViewController, IndicatorInfoProvider {
    static let IDENTIFIER = "No Camera"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Foto"
    }
}

class ColorsViewController: ViewController {
    static let IDENTIFIER = "Colors"
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    
    var colors: [UIColor]!
    var defaultColors: [(String, UIColor, String)]! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defaultColors.append((
            "Verde"
            , UIColor(netHex: 0x00B200)
            , "Bom pra isso e aquilo..."
        ))
        
        var i = 0
        var j = 1
        while i < colors.count {
            while j < colors.count {
                if colors[i].compare(colors[j]) {
                    colors.removeAtIndex(j)
                }
                j = j + 1
            }
            i = i + 1
        }
        
        stackHeight.constant = stackHeight.constant * CGFloat(colors.count)
        
        for c in colors {
            for d in defaultColors {
                if c.compare(d.1) {
                    let view = UINib(nibName: "ColorView", bundle: nil).instantiateWithOwner(self
                        , options: nil)[0] as! ColorView
                    view.titleColor.text = d.0
                    view.descriptionColor.text = d.2
                    self.stack.addArrangedSubview(view)
                }
            }
        }
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}