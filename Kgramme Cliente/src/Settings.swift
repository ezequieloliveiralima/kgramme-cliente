//
//  Settings.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 25/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class EditProfileViewController: ViewController {
    @IBOutlet weak var textFullName : UITextField!
    @IBOutlet weak var textEmail    : UITextField!
    @IBOutlet weak var textCity     : UITextField!
    @IBOutlet weak var dateBirth    : UIDatePicker!
    @IBOutlet weak var textHeight   : UITextField!
    @IBOutlet weak var textWeight   : UITextField!
    @IBOutlet weak var textTarget   : UITextView!
    @IBOutlet weak var btnAvatar    : UIButton!
    
    let placeholder = "Descreva seu objetivo."
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
    }
    
    @IBAction func changeImage(sender: UIButton) {
        let actionSheet = UIAlertController(title: "Adicionar uma imagem"
            , message: "Selecione a origem"
            , preferredStyle: .ActionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera"
            , style: .Default
            , handler: { (_) in
                if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                    self.imagePicker.sourceType = .Camera
                    dispatch_async(dispatch_get_main_queue(), {
                        self.presentViewController(self.imagePicker, animated: true, completion: nil)
                    })
                }
        }))
        actionSheet.addAction(UIAlertAction(title: "Bibliteca de Imagens"
            , style: .Default
            , handler: { (_) in
                self.imagePicker.sourceType = .PhotoLibrary
                dispatch_async(dispatch_get_main_queue(), {
                    self.presentViewController(self.imagePicker, animated: true, completion: nil)
                })
        }))
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
}

extension EditProfileViewController: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        textView.textColor = UIColor.blackColor()
        if textView.text == placeholder {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = UIColor(netHex: 0xCCCCCC)
        }
    }
}

extension EditProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.btnAvatar.setImage(image, forState: .Normal)
            // Server.profile!.image = image.toPFFile()
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
}

class SettingsTableViewController: UITableViewController {
    static let IDENTIFIER = "Settings"
    
    @IBOutlet weak var textFullName : UITextField!
    @IBOutlet weak var textEmail    : UITextField!
    @IBOutlet weak var textCity     : UITextField!
    @IBOutlet weak var dateBirth    : UIDatePicker!
    @IBOutlet weak var textHeight   : UITextField!
    @IBOutlet weak var textWeight   : UITextField!
    @IBOutlet weak var textTarget   : UITextView!
    
    @IBOutlet weak var btnAvatar    : UIButton!
    
    let placeholder = "Descreva seu objetivo."
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.allowsEditing = true
        
        //        Server.customer?.fetchInBackgroundWithBlock({ (obj, error) in
        //            if error == nil {
        //                let customer = Server.customer!
        //                dispatch_async(dispatch_get_main_queue(), {
        //                    self.textHeight.text = "\(customer.height.doubleValue)"
        //                    self.textWeight.text = "\(customer.weight.doubleValue)"
        //                    self.textTarget.text = "\(customer.target)"
        //                    self.dateBirth.setDate(customer.birthDate ?? NSDate(), animated: true)
        //                    if self.textTarget.text == "" {
        //                        self.textTarget.textColor = UIColor(netHex: 0xCCCCCC)
        //                        self.textTarget.text = self.placeholder
        //                    }
        //                })
        //            }
        //        })
        //
        //        Server.profile?.fetchInBackgroundWithBlock({ (obj, error) in
        //            if error == nil {
        //                let profile = Server.profile!
        //                if let image = profile.image {
        //                    image.getDataInBackgroundWithBlock({ (data, error) in
        //                        if data != nil {
        //                            dispatch_async(dispatch_get_main_queue(), {
        //                                self.btnAvatar.setBackgroundImage(UIImage(data: data!), forState: .Normal)
        //                            })
        //                        }
        //                    })
        //                }
        //
        //                profile.user.fetchInBackgroundWithBlock({ (obj, error) in
        //                    dispatch_async(dispatch_get_main_queue(), {
        //                        self.textFullName.text  = profile.fullName
        //                        self.textEmail.text     = profile.user.email
        //                        self.textCity.text      = profile.city
        //                    })
        //                })
        //            }
        //        })
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.section == 3 && indexPath.row == 0 {
            //            if textFullName.text == ""
            //                || textCity.text == ""
            //                || textTarget.text == ""
            //                || textWeight.text == ""
            //                || textHeight.text == "" {
            //                let alert = UIAlertController(title: "Erro"
            //                    , message: "Preencha todos os dados."
            //                    , preferredStyle: .Alert)
            //                alert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: nil))
            //                self.presentViewController(alert, animated: true, completion: nil)
            //                return
            //            }
            
            //            let profile = Server.profile!
            //            let customer = Server.customer!
            //
            //            profile.fullName    = textFullName.text!
            //            profile.city        = textCity.text!
            //            customer.birthDate  = dateBirth.date
            //            customer.target     = textTarget.text
            //            customer.weight     = NSNumber(double: Double(textWeight.text!) ?? 0.0)
            //            customer.height     = NSNumber(double: Double(textHeight.text!) ?? 0.0)
            //
            //            profile.saveInBackgroundWithBlock({ (success, error) in
            //                guard error == nil else {
            //                    print(error)
            //                    return
            //                }
            //                customer.saveInBackgroundWithBlock({ (success, error) in
            //                    if NSUserDefaults.standardUserDefaults().boolForKey("first-use") {
            //                        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "first-use")
            //                        NSUserDefaults.standardUserDefaults().synchronize()
            //                        dispatch_async(dispatch_get_main_queue(), {
            //                            self.dismissViewControllerAnimated(true, completion: nil)
            //                        })
            //                    } else {
            //                        dispatch_async(dispatch_get_main_queue(), {
            //                            self.navigationController?.popViewControllerAnimated(true)
            //                        })
            //                    }
            //                })
            //            })
        }
}


}