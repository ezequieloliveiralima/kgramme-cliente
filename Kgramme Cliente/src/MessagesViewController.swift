//
//  MessagesViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 28/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit

protocol MessageTableViewCellDelegate {
    func view(cell: MessageTableViewCell)
    func ignore(cell: MessageTableViewCell)
}

class MessageTableViewCell: UITableViewCell {
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var progress: UIProgressView!
    
    var delegate: MessageTableViewCellDelegate?
    
    @IBAction func view(sender: UIButton) {
        delegate?.view(self)
    }
    
    @IBAction func ignore(sender: UIButton) {
        delegate?.ignore(self)
    }
}

class MessagesViewController: ViewController {
    static let IDENTIFIER = "Messages"
    
    @IBOutlet weak var tableView: UITableView!
    
    var messages: [Message]! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Server.getMessages { (messages) in
            self.messages = messages as! [Message]
            self.tableView.reloadData()
        }
    }
}

extension MessagesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        
        var cell: MessageTableViewCell!
        
        if message["type"] as! String == "simple" {
            cell = tableView.dequeueReusableCellWithIdentifier("") as! MessageTableViewCell
        } else if message["type"] as! String == "progress" {
            cell = tableView.dequeueReusableCellWithIdentifier("") as! MessageTableViewCell
        } else {
            cell = tableView.dequeueReusableCellWithIdentifier("") as! MessageTableViewCell
        }
        
        return cell
    }
}