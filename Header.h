//
//  Header.h
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 23/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

#include <stdint.h>
#include "INVector3.h"
#include "ColorSpaceConversion.h"


extern uint64_t dispatch_benchmark(size_t count, void (^block)(void));