//
//  ext.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 22/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit
import Parse

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    func compare(color: UIColor) -> Bool {
        let tolerance = 0.4
        var r1 = CGFloat(0.0)
        , g1 = CGFloat(0.0)
        , b1 = CGFloat(0.0)
        , a1 = CGFloat(0.0)
        , r2 = CGFloat(0.0)
        , g2 = CGFloat(0.0)
        , b2 = CGFloat(0.0)
        , a2 = CGFloat(0.0)
        
        self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        
        return fabs(Double(r1) - Double(r2)) <= tolerance
            && fabs(Double(g1) - Double(g2)) <= tolerance
            && fabs(Double(b1) - Double(b2)) <= tolerance
            && fabs(Double(a1) - Double(a2)) <= tolerance
    }
}

extension UIViewController {
    func getScreenBounds() -> CGSize {
        return UIScreen.mainScreen().bounds.size
    }
}

extension UIImage {
    func toPFFile() -> PFFile {
        let file = PFFile(data: UIImageJPEGRepresentation(self, 0.6)!)
        return file!
    }
}

extension CALayer {
    var borderUIColor: UIColor {
        set {
            self.borderColor = newValue.CGColor
        }
        
        get {
            return UIColor(CGColor: self.borderColor!)
        }
    }
}

extension UIView {
    var imageNamed: String {
        set {
            self.backgroundColor = UIColor(patternImage: UIImage(named: imageNamed)!)
        }
        
        get {
            return getImageNamed()
        }
    }
    
    func getImageNamed() -> String {
        return "bg1"
    }
}