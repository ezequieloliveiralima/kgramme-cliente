//
//  Parse.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 22/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import Foundation
import Parse

class Server {
    static let limit = 8
    
    typealias ServerSuccess = (AnyObject?)->Void
    static var profile: Profile?
    static var customer: Customer?
    
    class func isLogged() -> Bool {
        let isLogged = PFUser.currentUser() != nil
        
        if isLogged {
            let query = PFQuery(className: "Profile")
            query.whereKey("user", equalTo: PFUser.currentUser()!)
            query.getFirstObjectInBackgroundWithBlock({ (obj, error) in
                if error == nil {
                    self.profile = obj as? Profile
                    let query1 = PFQuery(className: "Customer")
                    query1.whereKey("profile", equalTo: profile!)
                    query1.getFirstObjectInBackgroundWithBlock({ (obj, error) in
                        if error == nil {
                            self.customer = obj as? Customer
                        } else {
                            print(error)
                        }
                    })
                } else {
                    print(error)
                }
            })
        }
        
        return isLogged
    }
    
    class func signIn(email: String, password: String, success: ServerSuccess?) {
        PFUser.logInWithUsernameInBackground(email, password: password) { (user, error) in
            if error == nil {
                dispatch_async(dispatch_get_main_queue(), {
                    success?(nil)
                })
            } else {
                NSNotificationCenter.defaultCenter().postNotificationName("error"
                    , object: nil
                    , userInfo: ["error": error!])
            }
        }
    }
    
    // MARK: Ok
    class func signUp(email: String, password: String, success: ServerSuccess?) {
        let user = PFUser()
        user.username = email
        user.email = email
        user.password = password
        
        user.signUpInBackgroundWithBlock { (_, error) in
            if error == nil {
                let profile = Profile()
                profile.user = user
                
                let custumer = Customer()
                custumer.profile = profile
                
                profile.saveInBackground()
                custumer.saveInBackground()
                
                self.profile = profile
                Server.customer = customer
                
                dispatch_async(dispatch_get_main_queue(), {
                    success?(nil)
                })
            } else {
                NSNotificationCenter.defaultCenter().postNotificationName("error"
                    , object: nil
                    , userInfo: ["error": error!])
            }
        }
    }
    
    class func tutors(success: ServerSuccess?) {
        let query = PFQuery(className: "Nutritionist")
        query.findObjectsInBackgroundWithBlock { (obj, error) in
            if error == nil {
                dispatch_async(dispatch_get_main_queue(), { 
                    success?(obj as! [Nutritionist])
                })
            }
        }
    }
    
    class func search(q: String, skip: Int, type: SearchType, success: ServerSuccess?) {
        switch type {
        case .Customers:
            Search.people(q, success: success)
        case .Feeds:
            Search.feeds(q, success: success)
        case .Recipes:
            Search.recipes(q, success: success)
        case .Tutors:
            Search.tutors(q, success: success)
        }
    }
    
    internal class Search {
        class func people(q: String, success: ServerSuccess?) {
            let query = PFQuery(className: "Profile")
            query.whereKey("fullName", containsString: q)
            query.findObjectsInBackgroundWithBlock { (results, error) in
                if error == nil {
                    query.skip += results!.count
                    dispatch_async(dispatch_get_main_queue(), {
                        success?(results)
                    })
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("error"
                        , object: nil
                        , userInfo: ["error": error!])
                }
            }
        }
        
        class func feeds(q: String, success: ServerSuccess?) {
            let query = PFQuery(className: "Post")
            query.whereKey("hashTags", containsString: q)
            query.findObjectsInBackgroundWithBlock { (results, error) in
                if error == nil {
                    query.skip += results!.count
                    dispatch_async(dispatch_get_main_queue(), {
                        success?(results)
                    })
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("error"
                        , object: nil
                        , userInfo: ["error": error!])
                }
            }
        }
        
        class func recipes(q: String, success: ServerSuccess?) {
            let query = PFQuery(className: "Recipe")
            query.whereKey("hashTags", containsString: q)
            query.findObjectsInBackgroundWithBlock { (results, error) in
                if error == nil {
                    query.skip += results!.count
                    dispatch_async(dispatch_get_main_queue(), {
                        success?(results)
                    })
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("error"
                        , object: nil
                        , userInfo: ["error": error!])
                }
            }
        }
        
        class func tutors(q: String, success: ServerSuccess?) {
            let query = PFQuery(className: "Nutritionist")
            query.whereKey("specialties", containsString: q)
            query.findObjectsInBackgroundWithBlock { (results, error) in
                if error == nil {
                    query.skip += results!.count
                    dispatch_async(dispatch_get_main_queue(), {
                        success?(results)
                    })
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("error"
                        , object: nil
                        , userInfo: ["error": error!])
                }
            }
        }
    }
    
    class func logout(success: ServerSuccess?) {
        PFUser.logOutInBackgroundWithBlock { (error) in
            if error == nil {
                dispatch_async(dispatch_get_main_queue(), {
                    success?(nil)
                })
            } else {
                NSNotificationCenter.defaultCenter().postNotificationName("error"
                    , object: nil
                    , userInfo: ["error": error!])
            }
        }
    }
    
    internal class Feeds {
        enum LoadType {
            case Global
            , Friends
        }
        
        class func load(type: LoadType, skip: Int, success: ServerSuccess?) {
            switch type {
            case .Global: global(skip, success: success)
            case .Friends: friends(skip, success: success)
            }
        }
        
        private class func global(skip: Int, success: ServerSuccess?) {
            let query = PFQuery(className: "Post")
            query.orderByDescending("createdAt")
            query.cachePolicy = .CacheElseNetwork
            query.findObjectsInBackgroundWithBlock { (objects, error) in
                if error == nil {
                    // query.skip += objects!.count
                    dispatch_async(dispatch_get_main_queue(), {
                        success?(objects as! [Post])
                    })
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("error"
                        , object: nil
                        , userInfo: ["error": error!])
                }
            }
        }
        
        private class func friends(skip: Int, success: ServerSuccess?) {
            let query = PFQuery(className: "Post")
            query.whereKey("user", containedIn: Server.profile!.friends)
            query.orderByDescending("createdAt")
            query.cachePolicy = .CacheElseNetwork
            query.findObjectsInBackgroundWithBlock { (objects, error) in
                if error == nil {
                    // query.skip += objects!.count
                    dispatch_async(dispatch_get_main_queue(), {
                        success?(objects as! [Post])
                    })
                } else {
                    NSNotificationCenter.defaultCenter().postNotificationName("error"
                        , object: nil
                        , userInfo: ["error": error!])
                }
            }
        }
    }
    
    class func posts(profile: Profile, success: ServerSuccess?) {
        let query = PFQuery(className: "Post")
        query.whereKey("user", equalTo: profile)
        query.findObjectsInBackgroundWithBlock { (objects, error) in
            if error == nil {
                query.skip += objects!.count
                var posts = [Post]()
                for o in objects as! [Post] {
                    if o.denounced.isEmpty {
                        posts.append(o)
                    }
                }
                dispatch_async(dispatch_get_main_queue(), {
                    success?(posts)
                })
            } else {
                NSNotificationCenter.defaultCenter().postNotificationName("error"
                    , object: nil
                    , userInfo: ["error": error!])
            }
        }
    }
    
    class func checkIsOnline(user: PFUser, success: ServerSuccess?) {
        PFCloud.callFunctionInBackground("checkIsOnline"
            , withParameters: ["user": user.objectId!]) { (response, error) in
                if error == nil {
                    dispatch_async(dispatch_get_main_queue(), {
                        success?(response as? Bool)
                    })
                }
        }
    }
    
    class func getProfile(success: ServerSuccess?) {
        let query = PFQuery(className: "Profile")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        query.getFirstObjectInBackgroundWithBlock { (obj, error) in
            self.profile = obj as? Profile
            success?(nil)
        }
    }
    
    class func getMessages(success: ServerSuccess?) {
        let query = PFQuery(className: "Message")
        query.whereKey("to", equalTo: Server.profile!)
        query.findObjectsInBackgroundWithBlock { (obj, error) in
            if error == nil {
                dispatch_async(dispatch_get_main_queue(), {
                    success?(obj as? [Message])
                })
            }
        }
    }
}