//
//  RecipesViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 24/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit
import CoreActionSheetPicker

enum RecipesSubGroupType {
    case Search
    , Vegetarian
    , WithoutGluten
    , StomachUpset
    , WeightLoss
    , GainingMass
    , HealthyLife
}

class HeaderCreateRecipeSection: UIView {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
}

class RecipeGroupCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}

class RecipeTableViewCell: UITableViewCell {
    @IBOutlet weak var imageRecipe: UIImageView!
    @IBOutlet weak var titleRecipe: UILabel!
    var recipe: Recipe!
}

class TextTableViewCell: UITableViewCell {
    @IBOutlet weak var textView: UITextView!
}

class RecipesViewController: ViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedType: RecipesSubGroupType!
    
    let mainGroups: [(String, UIImage, RecipesSubGroupType)] = [
        ("Comida Vegetariana Vegana", UIImage(named: "vegetariana")!, .Vegetarian)
        , ("Sem lactose Sem gluten", UIImage(named: "sem-lactose")!, .WithoutGluten)
        , ("Comida para estômago irritado", UIImage(named: "estomago-irritado")!, .StomachUpset)
        , ("Perdendo Peso", UIImage(named: "perder-peso")!, .WeightLoss)
        , ("Ganhando Massa", UIImage(named: "massa")!, .GainingMass)
        , ("Vida saudável", UIImage(named: "saudavel")!, .HealthyLife)
    ]
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func search(sender: UIBarButtonItem) {
        selectedType = .Search
        self.performSegueWithIdentifier("Select Group", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Select Group" {
            (segue.destinationViewController as! RecipesSubGroup).type = selectedType
        }
    }
    
    @IBAction func createRecipe(sender: UIBarButtonItem) {
        let nextVC = storyboard!.instantiateViewControllerWithIdentifier(CreateRecipeViewController.IDENTIFIER)
            as! CreateRecipeViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}

extension RecipesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainGroups.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("recipe-cell", forIndexPath: indexPath) as! RecipeGroupCollectionViewCell
        
        cell.lblTitle.text = mainGroups[indexPath.row].0
        cell.imageView.image = mainGroups[indexPath.row].1
        cell.lblTitle.font = UIFont(name: "Eras Medium ITC", size: 20.0)!
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        self.selectedType = mainGroups[indexPath.row].2
        self.performSegueWithIdentifier("Select Group", sender: self)
    }
    
//    func collectionView(collectionView: UICollectionView
//        , layout collectionViewLayout: UICollectionViewLayout
//        , sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        
////        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
////        let totalSpace = flowLayout.sectionInset.left
////            + flowLayout.sectionInset.right
////            + (flowLayout.minimumInteritemSpacing * CGFloat(2 - 1))
////        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(2))
//        return CGSize(width: 165, height: 175)
//    }
}

class RecipesSubGroup: ViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var toolBar: UIToolbar!
    
    var type: RecipesSubGroupType!
    var recipes: [Recipe]! = []
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type == .Search {
            let nextVC = storyboard!.instantiateViewControllerWithIdentifier(SearchViewController.IDENTIFIER)
                as! SearchViewController
            nextVC.delegate = self
            nextVC.type = .Recipes
            nextVC.skip = 0
            self.presentViewController(nextVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}

extension RecipesSubGroup: SearchViewControllerDelegate {
    func searchViewController(results: [AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.recipes = results as! [Recipe]
        self.tableView.reloadData()
    }
}

extension RecipesSubGroup: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("recipe-cell") as! RecipeTableViewCell
        
        let recipe = recipes[indexPath.row]
        cell.recipe = recipe
        
        return cell
    }
}

class CreateRecipeViewController: ViewController {
    static let IDENTIFIER = "Create Recipe"
    
    @IBOutlet weak var tableView: UITableView!

    var type: RecipesSubGroupType!
    var newRecipe: Recipe! = Recipe()
    
    var ingredients = NSMutableArray()
    var directions = NSMutableArray()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44.0
        imagePicker.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func finish(sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Adicione uma foto"
            , message: "Selecione a fonte"
            , preferredStyle: .ActionSheet)
        alert.addAction(UIAlertAction(title: "Camera"
            , style: .Default
            , handler: { (_) in
                if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                    self.imagePicker.sourceType = .Camera
                } else {
                    self.imagePicker.sourceType = .PhotoLibrary
                }
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Biblioteca"
            , style: .Default
            , handler: { (_) in
                self.imagePicker.sourceType = .PhotoLibrary
                self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func edit(sender: UIBarButtonItem) {
        tableView.editing = !tableView.editing
    }
}

extension CreateRecipeViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController
        , didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.newRecipe.image = image.toPFFile()
        }
        
        dismissViewControllerAnimated(true, completion: nil)
        
        let alert = UIAlertController(title: "Finalizando..."
            , message: "Preencha os dados abaixo. Seja bem claro nas hashtags, os outros usuários usarão elas para pesquisar."
            , preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Adicione um titulo"
        }
        alert.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Adicione hashtags de acordo com o grupo de receitas"
        }
        alert.addAction(UIAlertAction(title: "Ok"
            , style: .Default
            , handler: { (_) in
                if alert.textFields![0].text != "" && alert.textFields![1].text != "" {
                    self.newRecipe.ingredients = self.ingredients.arrayByAddingObjectsFromArray([]) as! [Payload]
                    self.newRecipe.directions = self.directions.arrayByAddingObjectsFromArray([]) as! [Payload]
                    self.newRecipe.title = alert.textFields![0].text!
                    self.newRecipe.hashTags = (alert.textFields![1].text?.characters.split{ $0 == " " }.map(String.init))!
                    self.newRecipe.saveInBackgroundWithBlock({ (success, error) in
                        if error == nil {
                            dispatch_async(dispatch_get_main_queue(), { 
                                self.navigationController?.popViewControllerAnimated(true)
                            })
                        } else {
                            NSNotificationCenter.defaultCenter().postNotificationName("error"
                                , object: nil
                                , userInfo: ["error": error!])
                        }
                    })
                }
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

extension CreateRecipeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? ingredients.count : directions.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("basic-cell")!
            
            let ingredient = ingredients[indexPath.row] as? Payload
            cell.textLabel?.text = ingredient!["name"] as? String
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("text-view-cell") as! TextTableViewCell
            let direction = directions[indexPath.row] as? Payload
            cell.textView.text = direction!["description"] as! String
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let ingredient = UINib(nibName: "HeaderCreateRecipeSection"
            , bundle: nil).instantiateWithOwner(self, options: nil)[0] as! HeaderCreateRecipeSection
        ingredient.title.text = "INGREDIENTES"
        ingredient.btnAdd.addTarget(self
            , action: #selector(CreateRecipeViewController.addIngredient(_:))
            , forControlEvents: .PrimaryActionTriggered)
        
        let direction = UINib(nibName: "HeaderCreateRecipeSection"
            , bundle: nil).instantiateWithOwner(self, options: nil)[0] as! HeaderCreateRecipeSection
        direction.title.text = "MODO DE PREPARO"
        direction.btnAdd.addTarget(self
            , action: #selector(CreateRecipeViewController.addDirection(_:))
            , forControlEvents: .PrimaryActionTriggered)
        
        return section == 0 ? ingredient : direction
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func addIngredient(sender: UIButton) {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("AddIngredient") as! AddIngredientViewController
        vc.back = self
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func addDirection(sender: UIButton) {
        let vc = storyboard!.instantiateViewControllerWithIdentifier("AddDirection") as! AddDirectionViewController
        vc.back = self
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.section == 1 ? UITableViewAutomaticDimension : 44.0
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle
        , forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 {
            self.ingredients.removeObjectAtIndex(indexPath.row)
        } else if indexPath.section == 2 {
            self.directions.removeObjectAtIndex(indexPath.row)
        }
        
        self.tableView.reloadSections(NSIndexSet(index: indexPath.section), withRowAnimation: .None)
    }
}

class AddIngredientViewController: UITableViewController {
    @IBOutlet weak var textIngredientName: UITextField!
    @IBOutlet weak var textIngredientQuantity: UITextField!
    @IBOutlet weak var btnUnityName: UIButton!
    
    var back: CreateRecipeViewController!
    var toSave: Bool = false
    
    override func dismissViewControllerAnimated(flag: Bool, completion: (() -> Void)?) {
        super.dismissViewControllerAnimated(flag, completion: completion)
        if toSave {
            let i: Payload = [
                "name"      : textIngredientName.text!
                , "quantity": Double(textIngredientQuantity.text!)!
                , "unity"   : btnUnityName.currentTitle!
            ]
            back.ingredients.addObject(i)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.row == 2 {
            if textIngredientQuantity.text == ""
                || textIngredientName.text == ""
                || btnUnityName.currentTitle == "Unidade" {
                let alert = UIAlertController(title: "Erro"
                    , message: "É necessário preencher todos os dados", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok"
                    , style: .Cancel
                    , handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                toSave = false
                return
            }
            toSave = true
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func selectUnity(sender: UIButton) {
        ActionSheetStringPicker.showPickerWithTitle("Selecione a unidade"
            , rows: [
                "Xicara"
                , "Colher Chá"
                , "Colher Café"
                , "Colher Áçucar"
                , "Gramas"
                , "Kilos"
                , "Miligramas"
                , "Mililitros"
                , "Litros"
                , "Á gosto"
            ]
            , initialSelection: 0
            , doneBlock: { (action, index, obj) in
                sender.setTitle(obj as? String, forState: .Normal)
            }, cancelBlock: { (_) in
                
            }, origin: self)
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}

class AddDirectionViewController: UITableViewController {
    @IBOutlet weak var textDirectionDescription: UITextView!
    @IBOutlet weak var textDirectionDuration: UITextField!
    @IBOutlet weak var btnUnityName: UIButton!
    
    var back: CreateRecipeViewController!
    var toSave: Bool = false
    let placeholder = "Digite o passo-a-passo..."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textDirectionDescription.textColor = UIColor(netHex: 0xCCCCCC)
        textDirectionDescription.text = placeholder
        
        tableView.estimatedRowHeight = 44.0
    }
    
    override func dismissViewControllerAnimated(flag: Bool, completion: (() -> Void)?) {
        super.dismissViewControllerAnimated(flag, completion: completion)
        if toSave {
            let d: Payload = [
                "description"   : textDirectionDescription.text
                , "duration"    : Double(textDirectionDuration.text!)!
                , "unity"       : btnUnityName.currentTitle!
            ]
            back.directions.addObject(d)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.row == 2 {
            if textDirectionDescription.text == ""
                || textDirectionDuration.text == ""
                || btnUnityName.currentTitle == "Unidade" {
                let alert = UIAlertController(title: "Erro"
                    , message: "É necessário preencher todos os dados", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok"
                    , style: .Cancel
                    , handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                toSave = false
                return
            }
            toSave = true
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    @IBAction func selectUnity(sender: UIButton) {
        ActionSheetStringPicker.showPickerWithTitle("Selecione a unidade"
            , rows: [
                "Minutos"
                , "Horas"
                , "Até dourar"
                , "Á gosto"
                , "Até congelar"
            ]
            , initialSelection: 0
            , doneBlock: { (action, index, obj) in
                sender.setTitle(obj as? String, forState: .Normal)
            }, cancelBlock: { (_) in
                
            }, origin: self)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 0 ? UITableViewAutomaticDimension : 44.0
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}

extension AddDirectionViewController: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        textView.textColor = UIColor.blackColor()
        if textView.text == placeholder {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = UIColor(netHex: 0xCCCCCC)
        }
    }
    
    func textViewDidChange(textView: UITextView) {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}