//
//  FeedsViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 23/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol FeedTableViewCellDelegate {
    func more(cell: FeedTableViewCell)
    func like(cell: FeedTableViewCell)
    func comment(cell: FeedTableViewCell, text: String)
}

class FeedTableViewCell: UITableViewCell {
    @IBOutlet weak var imagePost: UIImageView!
    @IBOutlet weak var contentPost: UITextView!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var titleUser: UILabel!
    @IBOutlet weak var textView: UITextView! {
        didSet {
            textView.delegate = self
        }
    }
    @IBOutlet weak var myImage: UIImageView!
    
    var delegate: FeedTableViewCellDelegate?
    
    var post: Post! {
        didSet {
            contentPost.text = post.body
            post.user.fetchIfNeededInBackgroundWithBlock { (obj, error) in
                if error == nil {
                    dispatch_async(dispatch_get_main_queue(), {
                        self.titleUser.text = self.post.user.fullName
                        if self.post.user.image != nil {
                            self.post.user.image!.getDataInBackgroundWithBlock { (data, error) in
                                if error == nil && data != nil {
                                    dispatch_async(dispatch_get_main_queue(), {
                                        self.imageAvatar.image = UIImage(data: data!)
                                    })
                                }
                            }
                        } else {
                            dispatch_async(dispatch_get_main_queue(), {
                                self.imageAvatar.image = UIImage(named: "no-image")
                            })
                        }
                    })
                }
            }
            post.image.getDataInBackgroundWithBlock { (data, error) in
                if error == nil && data != nil {
                    dispatch_async(dispatch_get_main_queue(), { 
                        self.imagePost.image = UIImage(data: data!)
                    })
                }
            }
        }
    }
    
    @IBAction func more(sender: UIButton) {
        delegate?.more(self)
    }
    
    @IBAction func like(sender: UIButton) {
        delegate?.like(self)
    }
    
    @IBAction func reblog(sender: UIButton) {
        
    }
}

extension FeedTableViewCell: UITextViewDelegate {
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            delegate?.comment(self, text: textView.text)
            textView.text = ""
        }
        return true
    }
}

class CommentTableViewCell: UITableViewCell {
    @IBOutlet weak var titleProfile: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var textContent: UITextView!
}

class FeedsViewController: ViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var feeds: [Post]! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "FeedTableViewCell", bundle: nil), forCellReuseIdentifier: "feed-cell")
        tableView.estimatedRowHeight = 400.0
        tableView.contentInset = UIEdgeInsets(top: 0
            , left: 0
            , bottom: 0
            , right: 0)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self
            , action: #selector(FeedsViewController.reloadFeeds(_:))
            , forControlEvents: .PrimaryActionTriggered)
        tableView.addSubview(refreshControl)
        
        Server.Feeds.load(.Global, skip: feeds.count) { (feeds) in
            self.feeds = feeds as! [Post]
            self.tableView.reloadData()
        }
    }
    
    func reloadFeeds(sender: UIRefreshControl) {
        self.feeds.removeAll()
        tableView.reloadData()
        sender.endRefreshing()
    }
    
    @IBAction func global(sender: UIBarButtonItem) {
//        Server.Feeds.load(.Global, skip: feeds.count) { (feeds) in
//            self.feeds = feeds as! [Post]
//            self.tableView.reloadData()
//        }
    }
    
    @IBAction func friends(sender: UIBarButtonItem) {
//        Server.Feeds.load(.Friends, skip: feeds.count) { (feeds) in
//            self.feeds = feeds as! [Post]
//            self.tableView.reloadData()
//        }
    }
    
    @IBAction func search(sender: UIBarButtonItem) {
        let nextVC = storyboard!.instantiateViewControllerWithIdentifier(SearchViewController.IDENTIFIER) as! SearchViewController
        nextVC.delegate = self
        nextVC.type = .Feeds
        nextVC.skip = feeds.count
        
        self.presentViewController(nextVC, animated: true, completion: nil)
    }
}

extension FeedsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: FeedTableViewCell!
        
        let feed = feeds[indexPath.row]
        cell = tableView.dequeueReusableCellWithIdentifier("feed-cell") as! FeedTableViewCell
        cell.post = feed
        cell.delegate = self

        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension FeedsViewController: FeedTableViewCellDelegate {
    func more(cell: FeedTableViewCell) {
        let more = UIAlertController(title: ""
            , message: ""
            , preferredStyle: .ActionSheet)
        more.addAction(UIAlertAction(title: "Cancelar"
            , style: .Cancel
            , handler: nil))
        more.addAction(UIAlertAction(title: "Denunciar"
            , style: .Default
            , handler: { (_) in
                // cell.post.denounced.append(Server.profile!.user)
                let indexPath = self.tableView.indexPathForCell(cell)
                self.tableView.beginUpdates()
                self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
                self.tableView.endUpdates()
        }))
        more.addAction(UIAlertAction(title: "Comentários"
            , style: .Default
            , handler: { (_) in
                let vc = self.storyboard!.instantiateViewControllerWithIdentifier("Comments") as! CommentsViewController
                vc.post = cell.post
                self.presentViewController(vc, animated: true, completion: nil)
        }))
        
        self.presentViewController(more, animated: true, completion: nil)
    }
    
    func like(cell: FeedTableViewCell) {
        if let i = cell.post.likes.indexOf(Server.profile!) {
            cell.post.likes.removeAtIndex(i)
            cell.post.saveInBackgroundWithBlock { (success, error) in
                if error == nil {
                    dispatch_async(dispatch_get_main_queue(), {
                        let alert = UIAlertController(title: ""
                            , message: "Você não gosta mais dessa publicação."
                            , preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Ok"
                            , style: .Cancel
                            , handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    })
                }
            }
        } else {
            cell.post.likes.append(Server.profile!)
            cell.post.saveInBackgroundWithBlock { (success, error) in
                if error == nil {
                    dispatch_async(dispatch_get_main_queue(), {
                        let alert = UIAlertController(title: ""
                            , message: "Você marcou que gostou dessa publicação."
                            , preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Ok"
                            , style: .Cancel
                            , handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    })
                }
            }
        }
    }
    
    func comment(cell: FeedTableViewCell, text: String) {
        let alert = UIAlertController(title: ""
            , message: "Deseja salvar este comentário?"
            , preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Não"
            , style: .Cancel
            , handler: nil))
        alert.addAction(UIAlertAction(title: "Sim"
            , style: .Default
            , handler: { (_) in
                let newComment = Comment()
                newComment.profile = Server.profile!
                newComment.content = text
                newComment.saveInBackground()
                cell.post.comments.append(newComment)
                cell.post.saveInBackground()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

extension FeedsViewController: SearchViewControllerDelegate {
    func searchViewController(results: [AnyObject]) {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.feeds = results as! [Post]
        self.tableView.reloadData()
    }
}

class PublishViewController: ViewController {
    static let IDENTIFIER = "Publish"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    
    let placeholder = "Digite algo que queira compartilhar."
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.textColor = UIColor(netHex: 0xCCCCCC)
        textView.text = placeholder
        
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func changeImage(sender: UIGestureRecognizer) {
        let actionSheet = UIAlertController(title: "Adicionar uma imagem"
            , message: "Selecione a origem"
            , preferredStyle: .ActionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera"
            , style: .Default
            , handler: { (_) in
                if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                    self.imagePicker.sourceType = .Camera
                    dispatch_async(dispatch_get_main_queue(), {
                        self.presentViewController(self.imagePicker, animated: true, completion: nil)
                    })
                }
        }))
        actionSheet.addAction(UIAlertAction(title: "Bibliteca de Imagens"
            , style: .Default
            , handler: { (_) in
                self.imagePicker.sourceType = .PhotoLibrary
                dispatch_async(dispatch_get_main_queue(), {
                    self.presentViewController(self.imagePicker, animated: true, completion: nil)
                })
        }))
        self.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func publish(sender: UIButton) {
        textView.resignFirstResponder()
        if imageView.image == nil {
            let alert = UIAlertController(title: "Erro"
                , message: "Selecione uma imagem"
                , preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok"
                , style: .Cancel
                , handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        let addHashTahs = UIAlertController(title: "Adicionar hashtags"
            , message: "Separe hashtags por espaço"
            , preferredStyle: .Alert)
        addHashTahs.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Ex: vidasaudavel"
        }
        addHashTahs.addAction(UIAlertAction(title: "Finalizar"
            , style: .Cancel
            , handler: { (action) in
                MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                
                let post = Post()
                post.image = self.imageView.image!.toPFFile()
//                post.user = Server.profile!
                post.body = self.textView.text
                post.hashTags = addHashTahs.textFields![0].text!.componentsSeparatedByString(" ")
                
                post.saveInBackgroundWithBlock { (success, error) in
                    dispatch_async(dispatch_get_main_queue(), {
                        MBProgressHUD.hideHUDForView(self.view, animated: true)
                        if error == nil {
                            self.dismissViewControllerAnimated(true, completion: nil)
                        } else {
                            print(error)
                        }
                    })
                }
        }))
        self.presentViewController(addHashTahs, animated: true, completion: nil)
    }
}

extension PublishViewController: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        textView.textColor = UIColor.blackColor()
        if textView.text == placeholder {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = UIColor(netHex: 0xCCCCCC)
        }
    }
}

extension PublishViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.imageView.image = image
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
}

class CommentsViewController: ViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var post: Post!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 44.0
        post.fetchInBackgroundWithBlock { (obj, error) in
            dispatch_async(dispatch_get_main_queue(), { 
                self.tableView.reloadData()
            })
        }
    }
    
    @IBAction func close(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension CommentsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return post.comments.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("comment-cell") as! CommentTableViewCell
        
        let comment = post.comments[indexPath.row]
        comment.fetchInBackgroundWithBlock { (obj, error) in
            if error == nil {
                dispatch_async(dispatch_get_main_queue(), { 
                    cell.textContent.text = comment.content
                })
                comment.profile.fetchInBackgroundWithBlock({ (obj, error) in
                    if error == nil {
                        comment.profile.image?.getDataInBackgroundWithBlock({ (data, error) in
                            if data != nil {
                                dispatch_async(dispatch_get_main_queue(), {
                                    cell.imageProfile.image = UIImage(data: data!)
                                })
                            }
                        })
                        dispatch_async(dispatch_get_main_queue(), { 
                            cell.titleProfile.text = comment.profile.fullName
                        })
                    }
                })
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.updateConstraints()
    }
}