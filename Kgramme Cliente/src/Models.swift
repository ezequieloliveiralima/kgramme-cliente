//
//  Models.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 23/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import Foundation
import Parse

typealias Payload = [String:AnyObject]

class Profile: PFObject, PFSubclassing {
    @NSManaged var image    : PFFile?
    @NSManaged var fullName : String
    @NSManaged var user     : PFUser
    @NSManaged var city     : String
    @NSManaged var friends  : [ Profile ]
    
    static func parseClassName() -> String {
        return "Profile"
    }
}

class Nutritionist: PFObject, PFSubclassing {
    @NSManaged var profile: Profile
    @NSManaged var specialties: [String]
    
    static func parseClassName() -> String {
        return "Nutritionist"
    }
}

class Customer: PFObject, PFSubclassing {
    @NSManaged var profile: Profile
    @NSManaged var birthDate: NSDate
    @NSManaged var height: NSNumber
    @NSManaged var weight: NSNumber
    @NSManaged var target: String
    @NSManaged var dietType: String
    @NSManaged var diseaes: [String]
    @NSManaged var activityLevel: String
    @NSManaged var sleepType: String
    @NSManaged var engaged: [Nutritionist]

    static func parseClassName() -> String {
        return "Customer"
    }
}

class Dish: PFObject, PFSubclassing {
    @NSManaged var image        : PFFile
    @NSManaged var costumer     : Customer
    @NSManaged var aditionalInfo: String
    @NSManaged var tutor        : Nutritionist
    
    static func parseClassName() -> String {
        return "Dish"
    }
}

class Post: PFObject, PFSubclassing {
    @NSManaged var user  : Profile
    @NSManaged var body  : String
    @NSManaged var image : PFFile
    @NSManaged var hashTags: [String]
    @NSManaged var denounced: [PFUser]
    @NSManaged var comments: [Comment]
    @NSManaged var likes: [Profile]
    @NSManaged var reBlog: [Profile]
    
    static func parseClassName() -> String {
        return "Post"
    }
}

class Comment: PFObject, PFSubclassing {
    @NSManaged var content: String
    @NSManaged var profile: Profile
    
    static func parseClassName() -> String {
        return "Comment"
    }
}

class Recipe: PFObject, PFSubclassing {
    @NSManaged var title: String
    @NSManaged var ingredients: [Payload]
    @NSManaged var directions: [Payload]
    @NSManaged var image: PFFile?
    @NSManaged var hashTags: [String]
 
    static func parseClassName() -> String {
        return "Recipe"
    }
}

class Message: PFObject, PFSubclassing {
    @NSManaged var from: Profile
    @NSManaged var to: Profile
    @NSManaged var content: Payload
    
    static func parseClassName() -> String {
        return "Message"
    }
}

class Plan: PFObject, PFSubclassing {
    @NSManaged var title: String
    @NSManaged var price: NSNumber
    @NSManaged var totalPhotos: NSNumber
    
    static func parseClassName() -> String {
        return "Plan"
    }
}

class AlimentarPlan: PFObject, PFSubclassing {
    @NSManaged var customer: Customer
    @NSManaged var toDo: String
    @NSManaged var addedBy: Nutritionist
    
    static func parseClassName() -> String {
        return "AlimentarPlan"
    }
}

class HistoricCustomer: PFObject, PFSubclassing {
    @NSManaged var customer: Customer
    @NSManaged var plan: Plan
    @NSManaged var paymentMethod: String
    @NSManaged var status: String
    
    static func parseClassName() -> String {
        return "HistoricCustomer"
    }
}