//
//  ProfileViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 25/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit

enum ProfileViewType {
    case Costumer
    , Nutritionist
}

class ProfileViewController: ViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var titleProfile: UILabel!
    @IBOutlet weak var cityProfile: UILabel!
    @IBOutlet weak var ageProfile: UILabel!
    
    @IBOutlet weak var contentProfile: UIView!
    
    var viewType: ProfileViewType! = .Costumer
    var currentProfile: Profile?
    var timeline: [Post]! = []
    var tutorProfile: UIView!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if viewType == .Nutritionist {
            tutorProfile = UINib(nibName: "TutorProfile", bundle: nil)
                .instantiateWithOwner(self, options: nil)[0] as! UIView
            tutorProfile.frame = contentProfile.frame
            //  contentProfile.removeFromSuperview()
            self.view.addSubview(tutorProfile)
        }
        
//        titleProfile.text = Server.profile!.fullName
//        cityProfile.text = Server.profile!.city
//        let date = NSDate()
//        let calendar = NSCalendar.currentCalendar()
//        let components = calendar.components([ .Year ], fromDate: date)
//        
//        let birthDate = Server.customer!.birthDate
//        let componentsBirth = calendar.components([ .Year ], fromDate: birthDate)
//        
//        Server.profile!.image?.getDataInBackgroundWithBlock({ (data, error) in
//            if data != nil {
//                dispatch_async(dispatch_get_main_queue(), { 
//                    self.imageProfile.image = UIImage(data: data!)
//                })
//            }
//        })
        
        /*Server.posts(currentProfile!) { (feeds) in
            self.timeline = feeds as! [Post]
            dispatch_async(dispatch_get_main_queue(), {
                self.tableView.reloadData()
            })
        }*/
        
        tableView.registerNib(UINib(nibName: "FeedTableViewCell", bundle: nil), forCellReuseIdentifier: "feed-cell")
        tableView.estimatedRowHeight = 400.0
        
        // ageProfile.text = "\(components.year - componentsBirth.year)"
    }
    
    @IBAction func publish(sender: UIButton) {
        let publish = storyboard!.instantiateViewControllerWithIdentifier(PublishViewController.IDENTIFIER)
        self.presentViewController(publish
            , animated: true
            , completion: nil)
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeline.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let feed = timeline[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("feed-cell") as! FeedTableViewCell
        cell.post = feed
        cell.delegate = self
        
        return cell
    }
}

extension ProfileViewController: FeedTableViewCellDelegate {
    func more(cell: FeedTableViewCell) {
        let more = UIAlertController(title: ""
            , message: ""
            , preferredStyle: .ActionSheet)
        more.addAction(UIAlertAction(title: "Cancelar"
            , style: .Cancel
            , handler: nil))
        more.addAction(UIAlertAction(title: "Denunciar"
            , style: .Default
            , handler: { (_) in
                // cell.post.denounced.append(Server.profile!.user)
                let indexPath = self.tableView.indexPathForCell(cell)
                cell.post.saveInBackground()
                self.tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)

                self.tableView.beginUpdates()
                self.tableView.endUpdates()
        }))
        
        self.presentViewController(more, animated: true, completion: nil)
    }
    
    func like(cell: FeedTableViewCell) {
        if let i = cell.post.likes.indexOf(Server.profile!) {
            cell.post.likes.removeAtIndex(i)
            cell.post.saveInBackgroundWithBlock { (success, error) in
                if error == nil {
                    dispatch_async(dispatch_get_main_queue(), {
                        let alert = UIAlertController(title: ""
                            , message: "Você não gosta mais dessa publicação."
                            , preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Ok"
                            , style: .Cancel
                            , handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    })
                }
            }
        } else {
            cell.post.likes.append(Server.profile!)
            cell.post.saveInBackgroundWithBlock { (success, error) in
                if error == nil {
                    dispatch_async(dispatch_get_main_queue(), {
                        let alert = UIAlertController(title: ""
                            , message: "Você marcou que gostou dessa publicação."
                            , preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Ok"
                            , style: .Cancel
                            , handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    })
                }
            }
        }
    }
    
    func comment(cell: FeedTableViewCell, text: String) {
        let alert = UIAlertController(title: ""
            , message: "Deseja salvar este comentário?"
            , preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Não"
            , style: .Cancel
            , handler: nil))
        alert.addAction(UIAlertAction(title: "Sim"
            , style: .Default
            , handler: { (_) in
                let newComment = Comment()
                newComment.profile = Server.profile!
                newComment.content = text
                newComment.saveInBackground()
                cell.post.comments.append(newComment)
                cell.post.saveInBackground()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}