//
//  SearchViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 24/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit

enum SearchType {
    case Feeds
    , Recipes
    , Tutors
    , Customers
}

protocol SearchViewControllerDelegate {
    func searchViewController(results: [AnyObject])
}

class SearchViewController: ViewController {
    static let IDENTIFIER = "Search"
    
    @IBOutlet weak var textView: UITextView!
    
    let placeholder = "Digite sua pesquisa..."
    var delegate: SearchViewControllerDelegate?
    var type: SearchType!
    var skip: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.textColor = UIColor(netHex: 0xCCCCCC)
        textView.text = placeholder
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func startSearch(sender: UIButton) {
        if placeholder == textView.text {
            let alert = UIAlertController(title: "Erro"
                , message: "Digite algum termo para pesquisar."
                , preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
//        Server.search(textView.text, skip: skip, type: type) { (objects) in
//            self.delegate?.searchViewController(objects as! [AnyObject])
//        }
    }
}

extension SearchViewController: UITextViewDelegate {
    func textViewDidBeginEditing(textView: UITextView) {
        textView.textColor = UIColor.blackColor()
        if textView.text == placeholder {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = UIColor(netHex: 0xCCCCCC)
        }
    }
}