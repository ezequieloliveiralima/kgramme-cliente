//
//  LoginViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 22/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit

protocol LoginViewControllerDelegate {
    func signInAction(sender: AnyObject)
    func signUpAction(sender: AnyObject)
}

class SignInCollectionViewCell: UICollectionViewCell {
    static let IDENTIFIER = "SIGNIN_CELL"
    
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    
    var delegate: LoginViewControllerDelegate?
    
    @IBAction func signIn(sender: UIButton) {
        delegate?.signInAction(self)
        
        textEmail.text = ""
        textPassword.text = ""
    }
}

class SignUpCollectionViewCell: UICollectionViewCell {
    static let IDENTIFIER = "SIGNUP_CELL"
    
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var textRePassword: UITextField!
    
    var delegate: LoginViewControllerDelegate?
    
    func clearTextFields() {
        delegate?.signUpAction(self)
        
        textPassword.text = ""
        textRePassword.text = ""
        textEmail.text = ""
    }
}

class LoginViewController: ViewController {
    enum LoginType {
        case SignIn
        , SignUp
    }
    
    @IBOutlet weak var indicatorLeft: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var button: UIButton!
    
    var currentView: LoginType = .SignIn
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if Server.isLogged() {
            let nextVC = UIStoryboard(name: "Costumer", bundle: nil).instantiateViewControllerWithIdentifier("Initial")
            self.presentViewController(nextVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func signIn(sender: UIButton) {
        currentView = .SignIn
        moveIndicatorSelected(60)
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        button.setTitle("Entrar com facebook", forState: .Normal)
    }
    
    @IBAction func signUp(sender: UIButton) {
        currentView = .SignUp
        moveIndicatorSelected(260)
        let indexPath = NSIndexPath(forRow: 1, inSection: 0)
        collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .CenteredHorizontally, animated: true)
        button.setTitle("Criar Conta", forState: .Normal)
        button.addTarget(self, action: #selector(LoginViewController.signup), forControlEvents: .PrimaryActionTriggered)
    }
    
    private func moveIndicatorSelected(i: CGFloat) {
        UIView.animateWithDuration(0.5) {
            self.indicatorLeft.constant = i
            self.view.layoutIfNeeded()
        }
    }
    
    func signup() {
        let cell = collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: 1, inSection: 0)) as! SignUpCollectionViewCell
        
        var messageError: String?
        
        if cell.textPassword.text != cell.textRePassword.text {
            messageError = "As senhas são diferentes."
        }
        
        if cell.textPassword.text == "" || cell.textRePassword.text == "" {
            messageError = "Insira uma senha segura."
        }
        
        if messageError != nil {
            let alert = UIAlertController(title: "Erro"
                , message: messageError
                , preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        Server.signUp(cell.textEmail.text!
        , password: cell.textPassword.text!) { (_) in
            dispatch_async(dispatch_get_main_queue(), {
                if Server.isLogged() {
                    
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: "first-use")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    
                    let nextVC = UIStoryboard(name: "Costumer"
                        , bundle: nil).instantiateViewControllerWithIdentifier("Initial")
                    self.presentViewController(nextVC, animated: true, completion: nil)
                }
            })
        }
        
        cell.clearTextFields()
    }
    
    @IBAction func aditionalButton(sender: UIButton) {
        switch currentView {
        case .SignIn:
            break
        case .SignUp:
            break
        }
    }
}

extension LoginViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        
        if indexPath.row == 0 {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier(SignInCollectionViewCell.IDENTIFIER
                , forIndexPath: indexPath)
            (cell as! SignInCollectionViewCell).delegate = self
        } else {
            cell = collectionView.dequeueReusableCellWithReuseIdentifier(SignUpCollectionViewCell.IDENTIFIER
                , forIndexPath: indexPath)
            (cell as! SignUpCollectionViewCell).delegate = self
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width
            , height: 350.0)
    }
}

extension LoginViewController: LoginViewControllerDelegate {
    func signInAction(sender: AnyObject) {
         let cell = sender as! SignInCollectionViewCell
        Server.signIn(cell.textEmail.text!, password: cell.textPassword.text!) { (_) in
            if Server.isLogged() {
                dispatch_async(dispatch_get_main_queue(), {
                    let nextVC = UIStoryboard(name: "Costumer"
                        , bundle: nil).instantiateViewControllerWithIdentifier("Initial")
                    self.presentViewController(nextVC, animated: true, completion: nil)
                })
            }
        }
    }
    
    func signUpAction(sender: AnyObject) {
        
    }
}