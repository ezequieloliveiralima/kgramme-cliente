//
//  AppDelegate.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 22/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit
import Parse
import XLPagerTabStrip

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        Parse.enableLocalDatastore()
        _ = ButtonBarPagerTabStripViewController(nibName: nil, bundle: nil)
        
        let config = ParseClientConfiguration() {
            $0.applicationId    = "LOgps4XjrZFt4EQhNZGHkwEeItxnlgCtkFMsewpC"
            $0.clientKey        = "7qN4v6GBYANLwRvPBvVN93pWFijk57udlgrINiCg"
            $0.server           = "http://localhost:8080/api/kgramme/"
        }
        
        // https://iori.co/api/kgramme
        
        Parse.initializeWithConfiguration(config)
        
        if PFUser.currentUser() != nil {
            PFUser.currentUser()!.fetchIfNeededInBackground()
            isOnline()
        }
        
        Profile.registerSubclass()
        Nutritionist.registerSubclass()
        Customer.registerSubclass()
        Dish.registerSubclass()
        Recipe.registerSubclass()
        Post.registerSubclass()
        Comment.registerSubclass()
        Message.registerSubclass()
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if PFUser.currentUser() != nil {
            PFCloud.callFunctionInBackground("isOffline", withParameters: ["user": PFUser.currentUser()!.objectId!])
        }
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        isOnline()
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        self.window?.endEditing(true)
    }
    
    func isOnline() {
        if PFUser.currentUser() != nil {
            PFCloud.callFunctionInBackground("addOnlineUser", withParameters: ["user": PFUser.currentUser()!.objectId!])
        }
    }
    
}

