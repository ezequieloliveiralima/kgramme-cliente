//
//  Navigation.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 23/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import MBProgressHUD

class ContainerViewController: ButtonBarPagerTabStripViewController {
    static let IDENTIFIER = "Container"
    let mainColor = UIColor(netHex: 0xD96258)
    
    override func viewDidLoad() {
        settings.style.buttonBarBackgroundColor = .whiteColor()
        settings.style.buttonBarItemBackgroundColor = .whiteColor()
        settings.style.selectedBarBackgroundColor = mainColor
        settings.style.buttonBarItemFont = .boldSystemFontOfSize(14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 1
        settings.style.buttonBarItemTitleColor = .blackColor()
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        settings.style.buttonBarItemFont = UIFont(name: "Eras Medium ITC", size: 14.0)!
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .blackColor()
            newCell?.label.textColor = self?.mainColor
        }
        
        super.viewDidLoad()
        self.edgesForExtendedLayout = .None
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let feeds = storyboard!.instantiateViewControllerWithIdentifier(NavigationFeeds.IDENTIFIER)
        var photo: UIViewController!
        
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            photo = storyboard!.instantiateViewControllerWithIdentifier(PhotoViewController.IDENTIFIER)
        } else {
            photo = storyboard!.instantiateViewControllerWithIdentifier(NoCameraViewController.IDENTIFIER)
        }
        
        let recipes = storyboard!.instantiateViewControllerWithIdentifier(NavigationRecipes.IDENTIFIER)
        let tutor = storyboard!.instantiateViewControllerWithIdentifier(NavigationTutor.IDENTIFIER) as! NavigationTutor
        let profile = storyboard!.instantiateViewControllerWithIdentifier(NavigationProfile.IDENTIFIER)
        
        (tutor.topViewController as! TutorViewController).container = self
        
        let array: [UIViewController] = [ feeds, photo, recipes, tutor, profile ]
        
        return array
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}

class MainMenu: UIView {
    @IBOutlet weak var btnSettings: UIButton!
    @IBOutlet weak var btnMessages: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
}

class NavigationController: UINavigationController {
    var mainMenu: MainMenu!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        if NSUserDefaults.standardUserDefaults().boolForKey("first-use") {
            let alert = UIAlertController(title: ""
                , message: "Agora, insira seus dados para concluirmos o seu cadastro."
                , preferredStyle: .Alert)
            
            alert.addAction(UIAlertAction(title: "Ok"
                , style: .Cancel
                , handler: { (_) in
                    let settings = self.storyboard!.instantiateViewControllerWithIdentifier(SettingsTableViewController.IDENTIFIER)
                    self.presentViewController(settings, animated: true, completion: nil)
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainMenu = UINib(nibName: "MainMenu", bundle: nil).instantiateWithOwner(self, options: nil)[0] as! MainMenu
        mainMenu.frame = self.navigationBar.frame
        
        mainMenu.btnLogout.addTarget(self
            , action: #selector(NavigationController.logout(_:))
            , forControlEvents: .PrimaryActionTriggered)
        
        mainMenu.btnMessages.addTarget(self
            , action: #selector(NavigationController.inbox(_:))
            , forControlEvents: .PrimaryActionTriggered)
        
        mainMenu.btnSettings.addTarget(self
            , action: #selector(NavigationController.settings(_:))
            , forControlEvents: .PrimaryActionTriggered)
        
        self.view.addSubview(mainMenu)
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        Server.getProfile { (_) in
            Server.profile?.fetchInBackgroundWithBlock({ (obj, error) in
                if error == nil {
                    dispatch_async(dispatch_get_main_queue(), {
                        MBProgressHUD.hideHUDForView(self.view, animated: true)
                        self.mainMenu.btnSettings.setTitle(Server.profile!.fullName, forState: .Normal)
                    })
                }
            })
        }
        
        self.navigationBar.removeFromSuperview()
    }
    
    func settings(sender: UIButton) {
        if topViewController is SettingsTableViewController {
            self.popViewControllerAnimated(true)
            return
        }
        let settings = storyboard!.instantiateViewControllerWithIdentifier(SettingsTableViewController.IDENTIFIER)
        self.pushViewController(settings, animated: true)
    }
    
    func inbox(sender: UIButton) {
        if topViewController is MessagesViewController {
            self.popViewControllerAnimated(true)
            return
        }
        let messages = storyboard!.instantiateViewControllerWithIdentifier(MessagesViewController.IDENTIFIER)
        self.pushViewController(messages, animated: true)
    }
    
    func logout(sender: UIButton) {
        let alert = UIAlertController(title: "Logout"
            , message: "Deseja sair?"
            , preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Sim"
            , style: .Default
            , handler: { (_) in
                Server.logout({ (_) in
                    dispatch_async(dispatch_get_main_queue(), { 
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                })
        }))
        alert.addAction(UIAlertAction(title: "Não"
            , style: .Cancel
            , handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

class NavigationFeeds: UINavigationController, IndicatorInfoProvider {
    static let IDENTIFIER = "Feeds"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Feeds"
    }
}

class NavigationRecipes: UINavigationController, IndicatorInfoProvider {
    static let IDENTIFIER = "Recipes"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Receitas"
    }
}

class NavigationTutor: UINavigationController, IndicatorInfoProvider {
    static let IDENTIFIER = "Tutor"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Tutor"
    }
}

class NavigationProfile: UINavigationController, IndicatorInfoProvider {
    static let IDENTIFIER = "Profile"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return "Perfil"
    }
}