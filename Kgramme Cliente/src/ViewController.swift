//
//  ViewController.swift
//  Kgramme Cliente
//
//  Created by Ezequiel de Oliveira Lima on 22/07/16.
//  Copyright © 2016 iori co. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self
            , selector: #selector(ViewController.handleError(_:))
            , name: "error"
            , object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self
            , selector: #selector(ViewController.keyboardDidShow(_:))
            , name: UIKeyboardDidShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self
            , selector: #selector(ViewController.keyboardDidHide(_:))
            , name: UIKeyboardDidHideNotification
            , object: nil)
    }
    
    func handleError(notification: NSNotification) {
        print("========= Erro =========")
        print(notification.userInfo)
    }
    
    func keyboardDidShow(notification: NSNotification) {
    }
    
    func keyboardDidHide(notification: NSNotification) {
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

}

